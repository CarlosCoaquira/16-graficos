¿Qué aprenderemos en esta sección?

Esta sección esta dedicada en enseñar a usar gráficas dinámicas en Angular.

Puntualmente veremos sobre:

    Instalaciones necesarias
    Configuraciones
    Gráfico de Barras
    Gráfico de Donas
    Gráfico de Radar

Espero que este contenido adicional sea de su agrado y es en respuesta a varios alumnos que han solicitado que agregara este contenido.

https://www.chartjs.org/
instalar-> https://valor-software.com/ng2-charts/

para resolver error con libreria "chartjs-plugin-annotation": https://www.npmjs.com/package/chartjs-plugin-annotation
-> npm install chartjs-plugin-annotation --save

para resolver error con libreria "chartjs-plugin-datalabels": https://www.npmjs.com/package/chartjs-plugin-datalabels
-> npm i chartjs-plugin-datalabels